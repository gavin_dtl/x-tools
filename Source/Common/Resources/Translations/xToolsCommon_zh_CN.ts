<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>xToolsAffixesComboBox</name>
    <message>
        <location filename="../../CommonUI/xToolsAffixesComboBox.cpp" line="15"/>
        <source>None</source>
        <translation>无</translation>
    </message>
</context>
<context>
    <name>xToolsBleDeviceInfoComboBox</name>
    <message>
        <location filename="../../CommonUI/xToolsBleDeviceInfoComboBox.cpp" line="108"/>
        <source>Error Occurred</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>xToolsDataStructure</name>
    <message>
        <location filename="../../Common/xToolsDataStructure.cpp" line="77"/>
        <source>BIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Common/xToolsDataStructure.cpp" line="78"/>
        <source>TEXT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Common/xToolsDataStructure.cpp" line="310"/>
        <location filename="../../Common/xToolsDataStructure.cpp" line="320"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Common/xToolsDataStructure.cpp" line="491"/>
        <source>Bin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Common/xToolsDataStructure.cpp" line="492"/>
        <source>Oct</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Common/xToolsDataStructure.cpp" line="493"/>
        <source>Dec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Common/xToolsDataStructure.cpp" line="494"/>
        <source>Hex</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>xToolsEscapeCharacterComboBox</name>
    <message>
        <location filename="../../CommonUI/xToolsEscapeCharacterComboBox.cpp" line="16"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>xToolsFlowControlComboBox</name>
    <message>
        <location filename="../../CommonUI/xToolsFlowControlComboBox.cpp" line="16"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsFlowControlComboBox.cpp" line="17"/>
        <source>Hardware</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsFlowControlComboBox.cpp" line="18"/>
        <source>Software</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>xToolsMainWindow</name>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="60"/>
        <source>&amp;File</source>
        <translation>文件(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="61"/>
        <source>&amp;Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="66"/>
        <source>&amp;Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="76"/>
        <source>&amp;Languages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="101"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="111"/>
        <source>User QQ Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="114"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="124"/>
        <source>Application Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="155"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="158"/>
        <source>Clear Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="164"/>
        <source>Open configuration floder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="176"/>
        <source>HDPI Policy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="182"/>
        <source>Round up for .5 and above</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="183"/>
        <source>Always round up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="184"/>
        <source>Always round down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="186"/>
        <source>Round up for .75 and above</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="187"/>
        <source>Don&apos;t round</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="205"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="268"/>
        <source>(Part of Qt Swiss Army knife)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="270"/>
        <source>Author: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="272"/>
        <source>Email: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="275"/>
        <source>Commit: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="279"/>
        <source>Date: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="282"/>
        <source>Copyright 2018-%1 x-tools-author(x-tools@outlook.com). All rights reserved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="283"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="289"/>
        <source>Reboot application to effective</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsMainWindow.cpp" line="290"/>
        <source>Need to reboot, reboot to effective now?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>xToolsParityComboBox</name>
    <message>
        <location filename="../../CommonUI/xToolsParityComboBox.cpp" line="16"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsParityComboBox.cpp" line="17"/>
        <source>Even</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsParityComboBox.cpp" line="18"/>
        <source>Odd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsParityComboBox.cpp" line="19"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CommonUI/xToolsParityComboBox.cpp" line="20"/>
        <source>Mark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>xToolsResponseOptionComboBox</name>
    <message>
        <location filename="../../CommonUI/xToolsResponseOptionComboBox.cpp" line="17"/>
        <source>Disable</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../CommonUI/xToolsResponseOptionComboBox.cpp" line="19"/>
        <source>Echo</source>
        <comment>widget</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../CommonUI/xToolsResponseOptionComboBox.cpp" line="20"/>
        <source>Always</source>
        <comment>widget</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../CommonUI/xToolsResponseOptionComboBox.cpp" line="21"/>
        <source>RxEqualReference</source>
        <comment>widget</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../CommonUI/xToolsResponseOptionComboBox.cpp" line="23"/>
        <source>RxContainReference</source>
        <comment>widget</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../CommonUI/xToolsResponseOptionComboBox.cpp" line="25"/>
        <source>RxDiscontainReference</source>
        <comment>widget</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
</context>
</TS>
